{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Parameters": {
      "VPCCIDR": {
          "Description": "VPC CIDR",
          "Type": "String",
          "Default": "10.200.0.0/16"
      },
      "VPCName": {
          "Description": "VPC Name",
          "Type": "String",
          "Default": ""
      },
      "cidrt1commonsvca": {
          "Description": "t1-commonsvc-a",
          "Type": "String",
          "Default": "10.200.101.0/24"
      },
      "cidrt1commonsvcb": {
          "Description": "t1-commonsvc-b",
          "Type": "String",
          "Default": "10.200.102.0/24"
      },
      "cidrt1commonsvcc": {
          "Description": "t1-commonsvc-c",
          "Type": "String",
          "Default": "10.200.103.0/24"
      },
      "cidrt2commonsvca": {
          "Description": "t2-commonsvc-a",
          "Type": "String",
          "Default": "10.200.11.0/24"
      },
      "cidrt2commonsvcb": {
          "Description": "t2-commonsvc-b",
          "Type": "String",
          "Default": "10.200.12.0/24"
      },
      "cidrt2commonsvcc": {
          "Description": "t2-commonsvc-c",
          "Type": "String",
          "Default": "10.200.13.0/24"
      }                  
    },
    "Resources": {
        "VPC" : {
          "Type" : "AWS::EC2::VPC",
          "Properties" : {
            "CidrBlock" : { "Ref": "VPCCIDR" },
            "EnableDnsSupport" : "true",
            "EnableDnsHostnames" : "true",
            "Tags" : [ {"Key" : "Name", "Value" : { "Ref" : "VPCName" } } ]
         }
        },
        "T1CommonSubnetA" : {
          "Type" : "AWS::EC2::Subnet",
          "Properties" : {
            "VpcId" : { "Ref" : "VPC" },
            "CidrBlock" : { "Ref": "cidrt1commonsvca" },
            "MapPublicIpOnLaunch" : true,
            "AvailabilityZone" : {
              "Fn::Select" : [ "0", { "Fn::GetAZs" : { "Ref" : "AWS::Region" } } ]
            },
            "Tags" : [ {"Key" : "Name", "Value" : "t1-commonsvc-a"} ]
          }
        },
        "T1CommonSubnetB" : {
          "Type" : "AWS::EC2::Subnet",
          "Properties" : {
            "VpcId" : { "Ref" : "VPC" },
            "CidrBlock" : { "Ref": "cidrt1commonsvcb" },
            "MapPublicIpOnLaunch" : true,
            "AvailabilityZone" : {
              "Fn::Select" : [ "1", { "Fn::GetAZs" : { "Ref" : "AWS::Region" } } ]
            },
            "Tags" : [ {"Key" : "Name", "Value" : "t1-commonsvc-b"} ]
          }
        },
        "T1CommonSubnetC" : {
          "Type" : "AWS::EC2::Subnet",
          "Properties" : {
            "VpcId" : { "Ref" : "VPC" },
            "CidrBlock" : { "Ref": "cidrt1commonsvcc" },
            "MapPublicIpOnLaunch" : true,
            "AvailabilityZone" : {
              "Fn::Select" : [ "2", { "Fn::GetAZs" : { "Ref" : "AWS::Region" } } ]
            },
            "Tags" : [ {"Key" : "Name", "Value" : "t1-commonsvc-c"} ]
          }
        },
        "T2CommonSubnetA" : {
          "Type" : "AWS::EC2::Subnet",
          "Properties" : {
            "VpcId" : { "Ref" : "VPC" },
            "CidrBlock" : { "Ref": "cidrt2commonsvca" },
            "AvailabilityZone" : {
              "Fn::Select" : [ "0", { "Fn::GetAZs" : { "Ref" : "AWS::Region" } } ]
            },
            "Tags" : [ {"Key" : "Name", "Value" : "t2-commonsvc-a"} ]
          }
        },
        "T2CommonSubnetB" : {
          "Type" : "AWS::EC2::Subnet",
          "Properties" : {
            "VpcId" : { "Ref" : "VPC" },
            "CidrBlock" : { "Ref": "cidrt2commonsvcb" },
            "AvailabilityZone" : {
              "Fn::Select" : [ "1", { "Fn::GetAZs" : { "Ref" : "AWS::Region" } } ]
            },
            "Tags" : [ {"Key" : "Name", "Value" : "t2-commonsvc-b"} ]
          }
        },
        "T2CommonSubnetC" : {
          "Type" : "AWS::EC2::Subnet",
          "Properties" : {
            "VpcId" : { "Ref" : "VPC" },
            "CidrBlock" : { "Ref": "cidrt2commonsvcc" },
            "AvailabilityZone" : {
              "Fn::Select" : [ "2", { "Fn::GetAZs" : { "Ref" : "AWS::Region" } } ]
            },
            "Tags" : [ {"Key" : "Name", "Value" : "t2-commonsvc-c"} ]
          }
        },
        "routetablepublica" : {
           "Type" : "AWS::EC2::RouteTable",
           "Properties" : {
              "VpcId" : { "Ref" : "VPC" },
              "Tags" : [ { "Key" : "Name", "Value" : "rtb-public-a" } ]
           }
        },
        "routetablepublicb" : {
           "Type" : "AWS::EC2::RouteTable",
           "Properties" : {
              "VpcId" : { "Ref" : "VPC" },
              "Tags" : [ { "Key" : "Name", "Value" : "rtb-public-b" } ]
           }
        },
        "routetablepublicc" : {
           "Type" : "AWS::EC2::RouteTable",
           "Properties" : {
              "VpcId" : { "Ref" : "VPC" },
              "Tags" : [ { "Key" : "Name", "Value" : "rtb-public-c" } ]
           }
        },
        "routetableprivatea" : {
           "Type" : "AWS::EC2::RouteTable",
           "Properties" : {
              "VpcId" : { "Ref" : "VPC" },
              "Tags" : [ { "Key" : "Name", "Value" : "rtb-private-a" } ]
           }
        },
        "routetableprivateb" : {
           "Type" : "AWS::EC2::RouteTable",
           "Properties" : {
              "VpcId" : { "Ref" : "VPC" },
              "Tags" : [ { "Key" : "Name", "Value" : "rtb-private-b" } ]
           }
        },
        "routetableprivatec" : {
           "Type" : "AWS::EC2::RouteTable",
           "Properties" : {
              "VpcId" : { "Ref" : "VPC" },
              "Tags" : [ { "Key" : "Name", "Value" : "rtb-private-c" } ]
           }
        },
        "InternetDefaultRouteA" : {
           "Type" : "AWS::EC2::Route",
           "DependsOn" : "InternetGatewayAttachment",
           "Properties" : {
              "RouteTableId" : { "Ref" : "routetablepublica" },
              "DestinationCidrBlock" : "0.0.0.0/0",
              "GatewayId" : { "Ref" : "InternetGateway" }
           }
        },
        "InternetDefaultRouteB" : {
           "Type" : "AWS::EC2::Route",
           "DependsOn" : "InternetGatewayAttachment",
           "Properties" : {
              "RouteTableId" : { "Ref" : "routetablepublicb" },
              "DestinationCidrBlock" : "0.0.0.0/0",
              "GatewayId" : { "Ref" : "InternetGateway" }
           }
        },
        "InternetDefaultRouteC" : {
           "Type" : "AWS::EC2::Route",
           "DependsOn" : "InternetGatewayAttachment",
           "Properties" : {
              "RouteTableId" : { "Ref" : "routetablepublicc" },
              "DestinationCidrBlock" : "0.0.0.0/0",
              "GatewayId" : { "Ref" : "InternetGateway" }
           }
        },
        "rtbAssocWebSubnet1" : {
           "Type" : "AWS::EC2::SubnetRouteTableAssociation",
           "Properties" : {
              "SubnetId" : { "Ref" : "T1CommonSubnetA" },
              "RouteTableId" : { "Ref" : "routetablepublica" }
           }
        },
        "rtbAssocWebSubnet2" : {
           "Type" : "AWS::EC2::SubnetRouteTableAssociation",
           "Properties" : {
              "SubnetId" : { "Ref" : "T1CommonSubnetB" },
              "RouteTableId" : { "Ref" : "routetablepublicb" }
           }
        },
        "rtbAssocWebSubnet3" : {
           "Type" : "AWS::EC2::SubnetRouteTableAssociation",
           "Properties" : {
              "SubnetId" : { "Ref" : "T1CommonSubnetC" },
              "RouteTableId" : { "Ref" : "routetablepublicc" }
           }
        },
        "rtbAssocAppSubnet1" : {
           "Type" : "AWS::EC2::SubnetRouteTableAssociation",
           "Properties" : {
              "SubnetId" : { "Ref" : "T2CommonSubnetA" },
              "RouteTableId" : { "Ref" : "routetableprivatea" }
           }
        },                        
        "rtbAssocAppSubnet2" : {
           "Type" : "AWS::EC2::SubnetRouteTableAssociation",
           "Properties" : {
              "SubnetId" : { "Ref" : "T2CommonSubnetB" },
              "RouteTableId" : { "Ref" : "routetableprivateb" }
           }
        },
        "rtbAssocAppSubnet3" : {
           "Type" : "AWS::EC2::SubnetRouteTableAssociation",
           "Properties" : {
              "SubnetId" : { "Ref" : "T2CommonSubnetC" },
              "RouteTableId" : { "Ref" : "routetableprivatec" }
           }
        },                                         
        "InternetGateway": {
            "Type": "AWS::EC2::InternetGateway",
            "Properties": {},
            "DependsOn": [
                "VPC"
            ]
        },
        "InternetGatewayAttachment": {
            "Type": "AWS::EC2::VPCGatewayAttachment",
            "Properties": {
                "InternetGatewayId": {
                    "Ref": "InternetGateway"
                },
                "VpcId": {
                    "Ref": "VPC"
                }
            }
        },
        "NATGatewayA" : {
          "DependsOn" : "InternetGatewayAttachment",
          "Type" : "AWS::EC2::NatGateway",
          "Properties" : {
            "AllocationId" : { "Fn::GetAtt" : ["EIPNatGatewayA", "AllocationId"]},
            "SubnetId" : { "Ref" : "T1CommonSubnetA"}
          }
        },
        "EIPNatGatewayA" : {
          "Type" : "AWS::EC2::EIP",
          "Properties" : {
            "Domain" : "vpc"
          }
        },
        "NatGatewayRouteA" : {
          "Type" : "AWS::EC2::Route",
          "Properties" : {
            "RouteTableId" : { "Ref" : "routetableprivatea" },
            "DestinationCidrBlock" : "0.0.0.0/0",
            "NatGatewayId" : { "Ref" : "NATGatewayA" }
          }
        },
        "NATGatewayB" : {
          "DependsOn" : "InternetGatewayAttachment",
          "Type" : "AWS::EC2::NatGateway",
          "Properties" : {
            "AllocationId" : { "Fn::GetAtt" : ["EIPNatGatewayB", "AllocationId"]},
            "SubnetId" : { "Ref" : "T1CommonSubnetB"}
          }
        },
        "EIPNatGatewayB" : {
          "Type" : "AWS::EC2::EIP",
          "Properties" : {
            "Domain" : "vpc"
          }
        },
        "NatGatewayRouteB" : {
          "Type" : "AWS::EC2::Route",
          "Properties" : {
            "RouteTableId" : { "Ref" : "routetableprivateb" },
            "DestinationCidrBlock" : "0.0.0.0/0",
            "NatGatewayId" : { "Ref" : "NATGatewayB" }
          }
        },
        "NATGatewayC" : {
          "DependsOn" : "InternetGatewayAttachment",
          "Type" : "AWS::EC2::NatGateway",
          "Properties" : {
            "AllocationId" : { "Fn::GetAtt" : ["EIPNatGatewayC", "AllocationId"]},
            "SubnetId" : { "Ref" : "T1CommonSubnetC"}
          }
        },
        "EIPNatGatewayC" : {
          "Type" : "AWS::EC2::EIP",
          "Properties" : {
            "Domain" : "vpc"
          }
        },
        "NatGatewayRouteC" : {
          "Type" : "AWS::EC2::Route",
          "Properties" : {
            "RouteTableId" : { "Ref" : "routetableprivatec" },
            "DestinationCidrBlock" : "0.0.0.0/0",
            "NatGatewayId" : { "Ref" : "NATGatewayC" }
          }
        },
        "MGTSecurityGroup" : {
        "Type" : "AWS::EC2::SecurityGroup",
        "Properties" : {
          "GroupName" : "g-management-access",
          "GroupDescription" : "allow access from management addresses",
          "SecurityGroupIngress" : [ 
            { "IpProtocol" : "-1", "CidrIp" : "10.200.0.0/16" }
          ],
          "VpcId" : { "Ref": "VPC" }
          }
      },                                                                    
    }
}